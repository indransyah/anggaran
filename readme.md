
## Requirements
- PHP
- Node
- MySQL


## How to Setup
1. Clone this repo
2. Install composer to install all PHP dependencies
https://getcomposer.org/download/

3. Install PHP dependencies:
`composer.phar install`


4. Install node js version 6.14.4
https://nodejs.org/en/download/

5. Install javascript dependencies:
`npm install`

6. Save as environment variable `.env.example` to `.env` and configure the database connection
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=dbname
DB_USERNAME=dbusername
DB_PASSWORD=dbpassword
```

7. Run `php artisan key:generate` to generate Laravel key
8. Run `php artisan passport:install` to make secret token for next step. Copy Client Secret token for Client ID 2.

```Encryption keys generated successfully.
Personal access client created successfully.
Client ID: 1
Client Secret: BqOEaD1mrkIrmPEhtdPKnomHB943UjZ7PkdkF5CP
Password grant client created successfully.
Client ID: 2
Client Secret: GwqGgZEId3aHzoBvnwkIPALRUWB1YhBXuayGb4np
```

9. Run database migration and seeder to create database tables and some default data:
`php artisan migrate --seed`

10. Save as environment configuration script `resources/assets/js/env.example.js` to `resources/assets/js/env.js` and configure API_URL and CLIENT_SECRET
```
export const API_URL = 'http://domainname.com'
export const CLIENT_SECRET = 'GwqGgZEId3aHzoBvnwkIPALRUWB1YhBXuayGb4np'
...
```

## How to Run
### Local Machine
1. Run PHP app: `php artisan serve`

2. Run JavaScript script generator:
`npm run dev`

## How to Upload
1. Stop JavaScript script generator (Ctrl + C)
2. Remove all files on `public/` folder
3. Run `npm run prod` to generate JavaScript file
4. Zip all files on root app folder and upload to your hosting.
5. If you only make changes on *.js file, just zip `public/` folder and upload and extract on hosting `public/` folder