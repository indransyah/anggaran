const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    mix.styles([
      'bootstrap.css', // node_modules/admin-lte/bootstrap/css/bootstrap.css
      'AdminLTE.css', // node_modules/admin-lte/dist/css/AdminLTE.css
      'skin-green.css', // node_modules/admin-lte/dist/css/skins/skin-green.css
      'SweetAlert.css',
      'vue-multiselect.min.css',
      'app.css'
    ]);
    mix.copy('node_modules/admin-lte/bootstrap/fonts', 'public/fonts')
    mix.copy('resources/assets/image', 'public/images')
    mix.scripts(['bootstrap.js', 'AdminLTE.js', 'SweetAlert.js', 'pdfmake.js', 'vfs_fonts.js'])
    mix.webpack('app.js');
});
