<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
  'middleware' => ['auth:api', 'acl'],
  'is' => 'developer|staff'
], function () {
  Route::get('bill', 'BillController@index');
  
  Route::get('user/info', 'UserController@info');
  Route::post('user/username', 'UserController@username');
  Route::get('user/{id}', 'UserController@show')->where('id', '[0-9]+');
  Route::put('user/{id}', 'UserController@update')->where('id', '[0-9]+');

  Route::get('period', 'PeriodController@index');
  Route::get('period/active', 'PeriodController@active');
  Route::get('period/check', 'PeriodController@check');
});

Route::group([
  'middleware' => ['auth:api', 'acl'],
  'is' => 'developer'
], function () {
  Route::get('bill/{id}', 'BillController@show')->where('id', '[0-9]+');
  Route::get('bill/code', 'BillController@code');
  Route::post('bill', 'BillController@store');
  Route::put('bill/{id}', 'BillController@update');
  Route::delete('bill/{id}', 'BillController@destroy');
  Route::get('bill/latest/{categoryid}', 'BillController@latest');
  
  Route::get('user', 'UserController@index');
  Route::post('user', 'UserController@store');
  Route::delete('user/{id}', 'UserController@destroy')->where('id', '[0-9]+');

  Route::get('departement', 'DepartementController@index');
  Route::get('departement/{id}', 'DepartementController@show')->where('id', '[0-9]+');
  Route::post('departement', 'DepartementController@store');
  Route::put('departement/{id}', 'DepartementController@update');
  Route::delete('departement/{id}', 'DepartementController@destroy');

  Route::post('period', 'PeriodController@store');
  Route::put('period/{id}', 'PeriodController@update')->where('id', '[0-9]+');
  Route::delete('period/{id}', 'PeriodController@destroy')->where('id', '[0-9]+');

  Route::get('period/revision/{id}', 'PeriodController@revision')->where('id', '[0-9]+');
});

Route::group([
  'middleware' => ['auth:api', 'acl'],
  'is' => 'staff'
], function () {

  Route::get('category', 'CategoryController@index');
  Route::post('category', 'CategoryController@store');
  Route::put('category/{id}', 'CategoryController@update');
  Route::delete('category/{id}', 'CategoryController@destroy');

  Route::get('bill/all', 'BillController@all');

  Route::get('budget', 'BudgetController@index');
  Route::get('budget/approval', 'BudgetController@approval');
  Route::get('budget/descendant/{id}', 'BudgetController@descendant')->where('id', '[0-9]+');
  Route::get('budget/{id}', 'BudgetController@show')->where('id', '[0-9]+');
  Route::post('budget', 'BudgetController@store');
  Route::post('budget/insert', 'BudgetController@insert');
  Route::get('budget/submit', 'BudgetController@submit');
  Route::get('budget/approve/{id}', 'BudgetController@approve')->where('id', '[0-9]+');
  Route::get('budget/reject/{id}', 'BudgetController@reject')->where('id', '[0-9]+');
  Route::put('budget/{id}', 'BudgetController@update')->where('id', '[0-9]+');
  Route::delete('budget/{id}', 'BudgetController@destroy');
  Route::get('budget/recap/{id}/{hasRevision?}', 'BudgetController@recap')->where(['id' => '[0-9]+', 'hasRevision' => '[0-9]+']);
  Route::get('budget/detail/{id}/{hasRevision?}', 'BudgetController@detail')->where(['id' => '[0-9]+', 'hasRevision' => '[0-9]+']);

  Route::get('departement/descendant', 'DepartementController@descendant');
  Route::get('departement/ancestor', 'DepartementController@ancestor');
  Route::get('departement/freeze', 'DepartementController@freeze');
  
  Route::post('note/{id}', 'NoteController@store')->where('id', '[0-9]+');

  Route::get('coordinator/{id}', 'CoordinatorController@index')->where('id', '[0-9]+');
  Route::post('coordinator/{id}', 'CoordinatorController@store')->where('id', '[0-9]+');
  Route::put('coordinator/{id}', 'CoordinatorController@update')->where('id', '[0-9]+');

  Route::get('recap/{id}', 'ReportController@recap')->where('id', '[0-9]+');
  Route::get('detail/{id}', 'ReportController@detail')->where('id', '[0-9]+');

  Route::get('notif', 'NotificationController@index');
});
