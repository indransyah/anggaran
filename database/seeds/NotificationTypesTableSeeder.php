<?php

use Illuminate\Database\Seeder;
use App\NotificationType;
use Illuminate\Support\Facades\Hash;

class NotificationTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      NotificationType::create([
        'name' => 'request',
        'message' => 'mengajukan rancangan anggaran'
      ]);

      NotificationType::create([
        'name' => 'reject',
        'message' => 'menolak pengajuan rancangan anggaran'
      ]);

      NotificationType::create([
        'name' => 'rollback',
        'message' => 'mengevaluasi kembali rancangan anggaran'
      ]);

      NotificationType::create([
        'name' => 'delete',
        'message' => 'menghapus sebuah rancangan anggaran'
      ]);

      NotificationType::create([
        'name' => 'newPeriod',
        'message' => 'memasuki tahun anggaran baru'
      ]);

      NotificationType::create([
        'name' => 'revPeriod',
        'message' => 'memasuki masa anggaran perubahan'
      ]);
    }
}
