<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Role::insert([
          [ 'name' => 'Developer', 'slug' => 'developer', 'description' => 'Pihak pengembang sistem' ],
          [ 'name' => 'Staff', 'slug' => 'staff', 'description' => 'Staff' ]
        ]);
    }
}
