<?php

use Illuminate\Database\Seeder;

class CategoryAndBillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
        * Category and Bill data in rekening.csv file
        */
        $file = database_path('seeds').'/rekening.csv';

        /**
        * Read and convert data in csv into array
        */
        $data = array_map('str_getcsv', file($file));

        /**
        * initial category
        */
        $initialCategory = [
          'id' => null,
          'code' => 0,
          'name' => ''
        ];
        $category = $initialCategory;

        /**
        * Initial transaction
        */
        DB::beginTransaction();

        /**
        * Insert every line of data in csv
        */
        foreach ($data as $key => $value) {
          try {
            if ($value[0] === '' && $value[1] === '') {
              /**
              * Reset category
              */
              $category = $initialCategory;
            } else if ($value[0] === '' && $value[1] !== '') {
              /**
              * Insert Category
              */
              $nameAndCode = explode('-', $value[1]);
              $category['name'] = trim(strtoupper($nameAndCode[0]));
              $category['code'] = trim($nameAndCode[1]);

              $createdCategory = \App\Bill::create([
                'code' => $category['code'],
                'name' => $category['name'],
              ]);
              $category['id'] = $createdCategory->id;
            } else {
              /**
              * Insert Bill
              */
              $bill = \App\Bill::create([
                'code' => trim($value[0]),
                'name' => trim($value[1]),
                'parent_id' => $category['id']
              ]);
            }

            if ($key === (count($data) - 1)) {
              /**
              * Commit db when reach the end of line
              */
              DB::commit();
            }
          } catch (\Exception $e) {
            /**
            * Rollback db when there is an error
            */
            DB::rollBack();
            die($e->getMessage());
          }
        }
    }
}
