<?php

use Illuminate\Database\Seeder;
use App\Departement;
use App\Role;
use App\User;

class DepartementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();

        $root = Departement::create([ 'code' => '0', 'name' => 'RSUD Temanggung', 'description' => '' ]);
        $developer = Departement::create([ 'code' => '', 'name' => 'Developer', 'description' => '' ]);

        /*
        $username = $faker->userName;
        $directur = User::create([
          'name' => $faker->name,
          'username' => $username,
          'password' => Hash::make($username)
        ]);
        $directur->assignRole(['directur', 'staff']);
        */

        $bidangKeperawatan = Departement::create([ 'code' => '1', 'name' => 'Bidang Keperawatan', 'description' => '', 'parent_id' => $root->id ]);
        $bidangPenunjang = Departement::create([ 'code' => '2', 'name' => 'Bidang Penunjang', 'description' => '', 'parent_id' => $root->id ]);
        $bidangPelayanan = Departement::create([ 'code' => '3', 'name' => 'Bidang Pelayanan', 'description' => '', 'parent_id' => $root->id ]);
        $bidangBagianUmum = Departement::create([ 'code' => '4', 'name' => 'Bidang Bagian Umum', 'description' => '', 'parent_id' => $root->id ]);
        /*
        $rootRoleSlug = [
          $bidangKeperawatan->code => strtolower(str_replace(' ', '-', $bidangKeperawatan->name)),
          $bidangPenunjang->code => strtolower(str_replace(' ', '-', $bidangPenunjang->name)),
          $bidangPelayanan->code => strtolower(str_replace(' ', '-', $bidangPelayanan->name)),
          $bidangBagianUmum->code => strtolower(str_replace(' ', '-', $bidangBagianUmum->name))
        ];
        Role::insert([
          [ 'name' => $bidangKeperawatan->name, 'slug' => $rootRoleSlug['1'], 'description' => $bidangKeperawatan->description, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ],
          [ 'name' => $bidangPenunjang->name, 'slug' => $rootRoleSlug['2'], 'description' => $bidangPenunjang->description, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ],
          [ 'name' => $bidangPelayanan->name, 'slug' => $rootRoleSlug['3'], 'description' => $bidangPelayanan->description, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ],
          [ 'name' => $bidangBagianUmum->name, 'slug' => $rootRoleSlug['4'], 'description' => $bidangBagianUmum->description, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ]
        ]);
        */
        /*
        foreach ($rootRoleSlug as $key => $value) {
          $username = $faker->userName;
          $user = User::create([
            'name' => $faker->name,
            'username' => $username,
            'password' => Hash::make($username)
          ]);
          $user->assignRole([ $value, 'staff' ]);
        }
        */

        // Bidang Keperawatan
        $seksiKeperawatanRawatInap = Departement::create([ 'code' => '1.1', 'name' => 'Seksi Keperawatan Rawat Inap', 'description' => '', 'parent_id' => $bidangKeperawatan->id ]);
        $seksiKeperawatanRawatJalan = Departement::create([ 'code' => '1.2', 'name' => 'Seksi Keperawatan Rawat Jalan', 'description' => '', 'parent_id' => $bidangKeperawatan->id ]);
        /*
        $bidangKeperawatanRoleSlug = [
          $seksiKeperawatanRawatInap->code => strtolower(str_replace(' ', '-', $seksiKeperawatanRawatInap->name)),
          $seksiKeperawatanRawatJalan->code => strtolower(str_replace(' ', '-', $seksiKeperawatanRawatJalan->name))
        ];
        Role::insert([
          [ 'name' => $seksiKeperawatanRawatInap->name, 'slug' => $bidangKeperawatanRoleSlug['1.1'], 'description' => $seksiKeperawatanRawatInap->description, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ],
          [ 'name' => $seksiKeperawatanRawatJalan->name, 'slug' => $bidangKeperawatanRoleSlug['1.2'], 'description' => $seksiKeperawatanRawatJalan->description, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ]
        ]);
        */
        /*
        foreach ($bidangKeperawatanRoleSlug as $key => $value) {
          $username = $faker->userName;
          $user = User::create([
            'name' => $faker->name,
            'username' => $username,
            'password' => Hash::make($username)
          ]);
          $user->assignRole([ $value, 'staff' ]);
        }
        */
        
        $seksiPenunjangMedis = Departement::create([ 'code' => '2.1', 'name' => 'Seksi Penunjang Medis', 'description' => '', 'parent_id' => $bidangPenunjang->id ]);
        $seksiPenunjangNonMedis = Departement::create([ 'code' => '2.2', 'name' => 'Seksi Penunjang Non Medis', 'description' => '', 'parent_id' => $bidangPenunjang->id ]);
        /*
        $bidangPenunjangRoleSlug = [
          $seksiPenunjangMedis->code => strtolower(str_replace(' ', '-', $seksiPenunjangMedis->name)),
          $seksiPenunjangNonMedis->code => strtolower(str_replace(' ', '-', $seksiPenunjangNonMedis->name))
        ];
        Role::insert([
          [ 'name' => $seksiPenunjangMedis->name, 'slug' => $bidangPenunjangRoleSlug['2.1'], 'description' => $seksiPenunjangMedis->description, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ],
          [ 'name' => $seksiPenunjangNonMedis->name, 'slug' => $bidangPenunjangRoleSlug['2.2'], 'description' => $seksiPenunjangNonMedis->description, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ]
        ]);
        */
        /*
        foreach ($bidangPenunjangRoleSlug as $key => $value) {
          $username = $faker->userName;
          $user = User::create([
            'name' => $faker->name,
            'username' => $username,
            'password' => Hash::make($username)
          ]);
          $user->assignRole([ $value, 'staff' ]);
        }
        */
        
        $seksiPelayananRawatInap = Departement::create([ 'code' => '3.1', 'name' => 'Seksi Pelayanan Rawat Inap', 'description' => '', 'parent_id' => $bidangPelayanan->id ]);
        $seksiPelayananRawatJalan = Departement::create([ 'code' => '3.2', 'name' => 'Seksi Pelayanan Rawat Jalan', 'description' => '', 'parent_id' => $bidangPelayanan->id ]);
        /*
        $bidangPelayananRoleSlug = [
          $seksiPelayananRawatInap->code => strtolower(str_replace(' ', '-', $seksiPelayananRawatInap->name)),
          $seksiPelayananRawatJalan->code => strtolower(str_replace(' ', '-', $seksiPelayananRawatJalan->name))
        ];
        Role::insert([
          [ 'name' => $seksiPelayananRawatInap->name, 'slug' => $bidangPelayananRoleSlug['3.1'], 'description' => $seksiPelayananRawatInap->description, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ],
          [ 'name' => $seksiPelayananRawatJalan->name, 'slug' => $bidangPelayananRoleSlug['3.2'], 'description' => $seksiPelayananRawatJalan->description, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ]
        ]);
        */
        /*
        foreach ($bidangPelayananRoleSlug as $key => $value) {
          $username = $faker->userName;
          $user = User::create([
            'name' => $faker->name,
            'username' => $username,
            'password' => Hash::make($username)
          ]);
          $user->assignRole([ $value, 'staff' ]);
        }
        */
        
        $subBagPerencanaan = Departement::create([ 'code' => '4.1', 'name' => 'Sub Bagian Perencanaan', 'description' => '', 'parent_id' => $bidangBagianUmum->id ]);
        $subBagKeuangan = Departement::create([ 'code' => '4.2', 'name' => 'Sub Bagian Keuangan', 'description' => '', 'parent_id' => $bidangBagianUmum->id ]);
        $subBagRTTU = Departement::create([ 'code' => '4.3', 'name' => 'Sub Bagian RTTU', 'description' => '', 'parent_id' => $bidangBagianUmum->id ]);
        /*
        $bidangBagianUmumRoleSlug = [
          $subBagPerencanaan->code => strtolower(str_replace(' ', '-', $subBagPerencanaan->name)),
          $subBagKeuangan->code => strtolower(str_replace(' ', '-', $subBagKeuangan->name)),
          $subBagRTTU->code => strtolower(str_replace(' ', '-', $subBagRTTU->name))
        ];
        Role::insert([
          [ 'name' => $subBagPerencanaan->name, 'slug' => $bidangBagianUmumRoleSlug['4.1'], 'description' => $subBagPerencanaan->description, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ],
          [ 'name' => $subBagKeuangan->name, 'slug' => $bidangBagianUmumRoleSlug['4.2'], 'description' => $subBagKeuangan->description, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ],
          [ 'name' => $subBagRTTU->name, 'slug' => $bidangBagianUmumRoleSlug['4.3'], 'description' => $subBagRTTU->description, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ]
        ]);
        */
        /*
        foreach ($bidangBagianUmumRoleSlug as $key => $value) {
          $username = $faker->userName;
          $user = User::create([
            'name' => $faker->name,
            'username' => $username,
            'password' => Hash::make($username)
          ]);
          $user->assignRole([ $value, 'staff' ]);
        }
        */

        $instalasiRawatInap = Departement::create([ 'code' => '1.1.1', 'name' => 'Instalasi Rawat Inap', 'description' => '', 'parent_id' => $seksiKeperawatanRawatInap->id ]);
        $komiteKeperawatan = Departement::create([ 'code' => '1.1.2', 'name' => 'Komite Keperawatan', 'description' => '', 'parent_id' => $seksiKeperawatanRawatInap->id ]);
        $instalasiMaternalPerinatal = Departement::create([ 'code' => '1.1.3', 'name' => 'Instalasi Maternal Perinatal', 'description' => '', 'parent_id' => $seksiKeperawatanRawatInap->id ]);
        /*
        $seksiKeperawatanRawatInapRoleSlug = [
          $instalasiRawatInap->code => strtolower(str_replace(' ', '-', $instalasiRawatInap->name)),
          $komiteKeperawatan->code => strtolower(str_replace(' ', '-', $komiteKeperawatan->name)),
          $instalasiMaternalPerinatal->code => strtolower(str_replace(' ', '-', $instalasiMaternalPerinatal->name))
        ];
        Role::insert([
          [ 'name' => $instalasiRawatInap->name, 'slug' => $seksiKeperawatanRawatInapRoleSlug['1.1.1'], 'description' => $instalasiRawatInap->description, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ],
          [ 'name' => $komiteKeperawatan->name, 'slug' => $seksiKeperawatanRawatInapRoleSlug['1.1.2'], 'description' => $komiteKeperawatan->description, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ],
          [ 'name' => $instalasiMaternalPerinatal->name, 'slug' => $seksiKeperawatanRawatInapRoleSlug['1.1.3'], 'description' => $instalasiMaternalPerinatal->description, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s") ]
        ]);
        */
        /*
        foreach ($seksiKeperawatanRawatInapRoleSlug as $key => $value) {
          $username = $faker->userName;
          $user = User::create([
            'name' => $faker->name,
            'username' => $username,
            'password' => Hash::make($username)
          ]);
          $user->assignRole([ $value, 'staff' ]);
        }
        */

        $IGD = Departement::create([ 'code' => '1.2.1', 'name' => 'IGD', 'description' => '', 'parent_id' => $seksiKeperawatanRawatJalan->id ]);
        $HD = Departement::create([ 'code' => '1.2.2', 'name' => 'HD', 'description' => '', 'parent_id' => $seksiKeperawatanRawatJalan->id ]);
        $IBS = Departement::create([ 'code' => '1.2.3', 'name' => 'IBS', 'description' => '', 'parent_id' => $seksiKeperawatanRawatJalan->id ]);
        $instalasiRawatJalan = Departement::create([ 'code' => '1.2.4', 'name' => 'Instalasi Rawat Jalan', 'description' => '', 'parent_id' => $seksiKeperawatanRawatJalan->id ]);

        $instalasiRadiologi = Departement::create([ 'code' => '2.1.1', 'name' => 'Instalasi Radiologi', 'description' => '', 'parent_id' => $seksiPenunjangMedis->id ]);
        $instalasiLaboratorium = Departement::create([ 'code' => '2.1.2', 'name' => 'Instalasi Laboratorium', 'description' => '', 'parent_id' => $seksiPenunjangMedis->id ]);
        $instalasiFarmasi = Departement::create([ 'code' => '2.1.3', 'name' => 'Instalasi Farmasi', 'description' => '', 'parent_id' => $seksiPenunjangMedis->id ]);
        $instalasiGizi = Departement::create([ 'code' => '2.1.4', 'name' => 'Instalasi Gizi', 'description' => '', 'parent_id' => $seksiPenunjangMedis->id ]);
        $instalasiBDRS = Departement::create([ 'code' => '2.1.5', 'name' => 'Instalasi BDRS', 'description' => '', 'parent_id' => $seksiPenunjangMedis->id ]);
        $instalasiRehabMedik = Departement::create([ 'code' => '2.1.6', 'name' => 'Instalasi Rehab Medik', 'description' => '', 'parent_id' => $seksiPenunjangMedis->id ]);

        $instalasiSanitasi = Departement::create([ 'code' => '2.2.1', 'name' => 'Instalasi Sanitasi', 'description' => '', 'parent_id' => $seksiPenunjangNonMedis->id ]);
        $IPSRS = Departement::create([ 'code' => '2.2.2', 'name' => 'IPSRS', 'description' => '', 'parent_id' => $seksiPenunjangNonMedis->id ]);
        $instalasiCSSD = Departement::create([ 'code' => '2.2.3', 'name' => 'Instalasi CSSD', 'description' => '', 'parent_id' => $seksiPenunjangNonMedis->id ]);
        $instalasiLaundry = Departement::create([ 'code' => '2.2.4', 'name' => 'Instalasi Laundry', 'description' => '', 'parent_id' => $seksiPenunjangNonMedis->id ]);

        $komiteMedik = Departement::create([ 'code' => '3.1.1', 'name' => 'Komite Medik', 'description' => '', 'parent_id' => $seksiPelayananRawatInap->id ]);

        $instalasiSIM = Departement::create([ 'code' => '4.1.1', 'name' => 'Instalasi SIM', 'description' => '', 'parent_id' => $subBagPerencanaan->id ]);
        $instalasiRekamMedis = Departement::create([ 'code' => '4.1.2', 'name' => 'Instalasi Rekam Medis', 'description' => '', 'parent_id' => $subBagPerencanaan->id ]);

        $instalasiPembayaran = Departement::create([ 'code' => '4.2.1', 'name' => 'Instalasi Pembayaran', 'description' => '', 'parent_id' => $subBagKeuangan->id ]);

        $unitGudang = Departement::create([ 'code' => '4.3.1', 'name' => 'Unit Gudang', 'description' => '', 'parent_id' => $subBagRTTU->id ]);
        $unitOperator = Departement::create([ 'code' => '4.3.2', 'name' => 'Unit Operator', 'description' => '', 'parent_id' => $subBagRTTU->id ]);
    }
}
