<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Departement;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $root = Departement::where('name', 'RSUD Temanggung')->first();
        $directur = User::create([
          'name' => 'Direktur RSUD Temanggung',
          'username' => 'directur',
          'password' => Hash::make('directur'),
          'departement_id' => $root->id
        ]);
        $directur->assignRole('staff');

        $dev = Departement::where('name', 'Developer')->first();
        $developer = User::create([
          'name' => 'Developer',
          'username' => 'developer',
          'password' => Hash::make('developer'),
          'departement_id' => $dev->id
        ]);
        $developer->assignRole('developer');

        $bidangKeperawatan = Departement::where('name', 'Bidang Keperawatan')->first();
        $bidangKeperawatanUser = User::create([
          'name' => 'Ahmad Fahmi',
          'username' => 'afahmi',
          'password' => Hash::make('afahmi'),
          'departement_id' => $bidangKeperawatan->id
        ]);
        $bidangKeperawatanUser->assignRole('staff');

        $seksiKeperawatanRawatInap = Departement::where('name', 'Seksi Keperawatan Rawat Inap')->first();
        $seksiKeperawatanRawatInapUser = User::create([
          'name' => 'Bintang',
          'username' => 'bintang',
          'password' => Hash::make('bintang'),
          'departement_id' => $seksiKeperawatanRawatInap->id
        ]);
        $seksiKeperawatanRawatInapUser->assignRole('staff');

        $instalasiRawatInap = Departement::where('name', 'Instalasi Rawat Inap')->first();
        $instalasiRawatInapUser = User::create([
          'name' => 'Rasyid',
          'username' => 'rasyid',
          'password' => Hash::make('rasyid'),
          'departement_id' => $instalasiRawatInap->id
        ]);
        $instalasiRawatInapUser->assignRole('staff');
    }
}
