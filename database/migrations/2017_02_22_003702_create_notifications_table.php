<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('departement_from')->unsigned();
            $table->integer('departement_to')->unsigned();
            $table->integer('notification_type')->unsigned();
            $table->timestamps();
            $table->foreign('departement_from')->references('id')->on('departements');
            $table->foreign('departement_to')->references('id')->on('departements');
            $table->foreign('notification_type')->references('id')->on('notification_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
