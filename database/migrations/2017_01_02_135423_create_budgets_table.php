<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budgets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('volume');
            $table->string('unit');
            $table->integer('price');
            $table->integer('total');
            $table->string('location')->nullable();
            $table->integer('period_id')->unsigned();
            $table->integer('bill_id')->unsigned()->nullable();
            $table->integer('departement_id')->unsigned();
            $table->integer('revision_id')->unsigned()->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->boolean('is_rejected');
            $table->boolean('is_additional');
            $table->timestamps();
            $table->foreign('period_id')->references('id')->on('periods')->onDelete('cascade');
            $table->foreign('bill_id')->references('id')->on('bills');
            $table->foreign('departement_id')->references('id')->on('departements');
            $table->foreign('revision_id')->references('id')->on('budgets')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budgets');
    }
}
