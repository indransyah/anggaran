<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('volume');
            $table->string('unit');
            $table->integer('price');
            $table->integer('total');
            $table->string('location')->nullable();
            $table->integer('bill_id')->unsigned()->nullable();
            $table->integer('departement_id')->unsigned();
            $table->integer('period_id')->unsigned();
            $table->foreign('bill_id')->references('id')->on('bills');
            $table->foreign('departement_id')->references('id')->on('departements');
            $table->foreign('period_id')->references('id')->on('periods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
