<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sistem Perencanaan Anggaran RSUD Temanggung</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="{{ elixir('css/all.css') }}">

</head>
<body class="hold-transition skin-green">
  <div v-cloak id="app">
    <router-view></router-view>
  </div>
<script type="text/javascript">
window.Laravel = <?php echo json_encode([
  'csrfToken' => csrf_token()
]); ?>
</script>
<script src="{{ elixir('js/app.js') }}"></script>
<script src="{{ elixir('js/all.js') }}"></script>

</body>
</html>
