export function getAuthorizationToken() {
  const auth = JSON.parse(window.localStorage.getItem('auth'))
  const token_type = auth.token_type || ''
  const access_token = auth.access_token || ''
  return {
    headers: {
      Authorization: `${token_type} ${access_token}`
    }
  }
}

export function galat(msg)
{
  swal(
    'Galat!',
    msg,
    'error'
  )
}

export function berhasil(msg)
{
  swal(
    'Berhasil!',
    msg,
    'success'
  )
}

export function rupiah(num)
{
  const	numString = num.toString()
	const sisa 	= numString.length % 3
	const ribuan 	= numString.substr(sisa).match(/\d{3}/g)
  let rupiah 	= numString.substr(0, sisa)
  if (ribuan) {
    const separator = sisa ? '.' : ''
    return `Rp ${rupiah}${separator}${ribuan.join('.')}`
  } else {
    return `Rp ${num}`
  }
}

export function ribuan(num)
{
  const numString = num.toString()
  const sisa  = numString.length % 3
  const ribuan  = numString.substr(sisa).match(/\d{3}/g)
  let rupiah  = numString.substr(0, sisa)
  if (ribuan) {
    const separator = sisa ? '.' : ''
    return `${rupiah}${separator}${ribuan.join('.')}`
  } else {
    return `${num}`
  }
}

export function getDataUri(url) {
  return new Promise(function(resolve, reject) {
    const image = new Image()
    image.onload = function () {
      const canvas = document.createElement('canvas')
      canvas.width = this.naturalWidth // or 'width' if you want a special/scaled size
      canvas.height = this.naturalHeight // or 'height' if you want a special/scaled size
      canvas.getContext('2d').drawImage(this, 0, 0)
      // Get raw image data
      // callback(canvas.toDataURL('image/png').replace(/^data:image\/(png|jpg);base64,/, ''))
      // ... or get as Data URI
      resolve(canvas.toDataURL('image/png'))
    };
    image.src = url
  })
}
