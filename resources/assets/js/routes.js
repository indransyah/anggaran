/**
* Components
*/
import Login from './components/Auth/Login.vue'
import Timeline from './components/Dashboard/Timeline.vue'
import AddBill from './components/Bill/AddBillNew.vue'
import EditBill from './components/Bill/EditBill.vue'
import Bills from './components/Bill/Bills.vue'
import NotFound from './components/404/NotFound.vue'
import Unauthorized from './components/401/Unauthorized.vue'
import AddBudget from './components/Budget/AddBudget.vue'
import Budgets from './components/Budget/Budgets.vue'
import ShowBudget from './components/Budget/ShowBudget.vue'
import BudgetApproval from './components/Budget/BudgetApproval.vue'
import ReportRecap from './components/Report/Recap.vue'
import ReportDetail from './components/Report/Detail.vue'
import AddDepartment from './components/Department/AddDepartment.vue'
import EditDepartment from './components/Department/EditDepartment.vue'
import Departments from './components/Department/Departments.vue'
import Periods from './components/Period/Periods.vue'
import AddUser from './components/User/AddUser.vue'
import Users from './components/User/Users.vue'
import Coordinator from './components/Department/DepartmentSigner.vue'
import Profile from './components/User/Profile.vue'

const routes = [
  { path: '/masuk', name: 'masuk', component: Login },
  { path: '/', name: 'beranda', component: Timeline,  meta: { requiresAuth: true } },
  { path: '/rekening/tambah', name: 'tambahRekening', component: AddBill,  meta: { requiresAuth: true } },
  { path: '/rekening', name: 'rekening', component: Bills, meta: { requiresAuth: true } },
  { path: '/rekening/sunting/:id', name: 'suntingRekening', component: EditBill, meta: { requiresAuth: true } },
  { path: '/anggaran/tambah', name: 'tambahAnggaran', component: AddBudget, meta: { requiresAuth: true } },
  { path: '/anggaran', name: 'anggaran', component: Budgets, meta: { requiresAuth: true } },
  { path: '/anggaran/:id', name: 'lihatAnggaran', component: ShowBudget, meta: { requiresAuth: true } },
  { path: '/persetujuan/anggaran', name: 'persetujuanAnggaran', component: BudgetApproval, meta: { requiresAuth: true } },
  { path: '/rekap/anggaran', name: 'rekapAnggaran', component: ReportRecap, meta: { requiresAuth: true } },
  { path: '/rekap/anggaran/detail', name: 'rekapDetailAnggaran', component: ReportDetail, meta: { requiresAuth: true } },
  { path: '/departemen/sunting/:id', name: 'suntingDepartemen', component: EditDepartment, meta: { requiresAuth: true } },
  { path: '/departemen/tambah', name: 'tambahDepartemen', component: AddDepartment, meta: { requiresAuth: true } },
  { path: '/departemen', name: 'departemen', component: Departments, meta: { requiresAuth: true } },
  { path: '/periode', name: 'periode', component: Periods, meta: { requiresAuth: true } },
  { path: '/pengguna/tambah', name: 'tambahPengguna', component: AddUser, meta: { requiresAuth: true } },
  { path: '/pengguna', name: 'pengguna', component: Users, meta: { requiresAuth: true } },
  { path: '/koordinator', name: 'koordinator', component: Coordinator, meta: { requiresAuth: true } },
  { path: '/profil', name: 'profil', component: Profile, meta: { requiresAuth: true } },
  { path: '/401', name: '401', component: Unauthorized },
  { path: '*', component: NotFound }
]

export default routes
