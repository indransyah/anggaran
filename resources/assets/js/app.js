import _ from 'lodash'
import jQuery from 'jquery'
import Vue from 'vue'
import VueResource from 'vue-resource'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import Swal from 'sweetalert'

import { API_URL } from './env'

/**
* Global jQuery for AdminLTE template
*/
window.$ = window.jQuery = jQuery

/**
* Vue plugins
*/
Vue.use(VueResource)
Vue.use(VueRouter)
Vue.use(Vuex)


/**
* Vue routes
*/
import routes from './routes'
const router = new VueRouter({
  routes
})

/**
* Router middleware
*/
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    const auth = JSON.parse(window.localStorage.getItem('auth'))
    if (auth && auth.access_token) {
      next()
    } else {
      next({ name: 'masuk' })
    }
  } else {
    next()
  }
})

/**
* Vue resource
*/
Vue.http.options.root = API_URL
Vue.http.headers.common['Accept'] = 'application/json'
// Vue.http.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'

/**
* Resource middleware
*/
Vue.http.interceptors.push((request, next)  => {
  /**
  * !IMPORTANT Check Laravel CSRF token
  */
  request.headers.set('X-CSRF-TOKEN', Laravel.csrfToken);

  next(function(response) {
    if (response.status === 401) {
      if (app.$route.name !== 'masuk') {
        Swal('Galat!', 'Anda tidak berhak melakukan request!', 'error')
        // router.push({ name: '401'})
      }
    }
  })
})

/**
* Vuex
*/
import store from './store'


/**
* Vue app
*/
const app = new Vue({
    el: '#app',
    router,
    store,
    updated() {
      window.$(function() {
        if (window.$.AdminLTE.layout) {
          window.$.AdminLTE.layout.activate()
        }
      })
    },
    watch: {
      '$route'(to) {
        console.log(to);
        if (to.name === 'masuk') {
          $('body').addClass('login-page')
        } else {
          $('body').removeClass('login-page')
        }
      }
    }
})
