const state = {
  isReportRecapLoading: false,
  isReportDetailLoading: false,
  reportDetail: [],
  reportRecap: [],
  reportRevisionDetail: [],
  reportRevisionRecap: [],
  isDetailLoading: false,
  isRecapLoading: false,
  isRevisionDetailLoading: false,
  isRevisionRecapLoading: false
}

const mutations = {
  TOGGLE_REPORT_RECAP_LOADING(state) {
    state.isReportRecapLoading = !state.isReportRecapLoading
  },
  TOGGLE_REPORT_DETAIL_LOADING(state) {
    state.isReportDetailLoading = !state.isReportDetailLoading
  },
  GET_REPORT_DETAIL(state, data) {
    state.reportDetail = [ ...data ]
  },
  GET_REPORT_REVISION_DETAIL(state, data) {
    state.reportRevisionDetail = [ ...data ]
  },
  GET_REPORT_RECAP(state, data) {
    state.reportRecap = [ ...data ]
  },
  GET_REPORT_REVISION_RECAP(state, data) {
    state.reportRevisionRecap = [ ...data ]
  },
  TOGGLE_DETAIL_LOADING(state) {
    state.isDetailLoading = !state.isDetailLoading
  },
  TOGGLE_RECAP_LOADING(state) {
    state.isRecapLoading = !state.isRecapLoading
  },
  TOGGLE_REVISION_DETAIL_LOADING(state) {
    state.isRevisionDetailLoading = !state.isRevisionDetailLoading
  },
  TOGGLE_REVISION_RECAP_LOADING(state) {
    state.isRevisionRecapLoading = !state.isRevisionRecapLoading
  }
}

const actions = {
  toggleReportRecapLoading({ commit }) {
    commit('TOGGLE_REPORT_RECAP_LOADING')
  },
  toggleReportDetailLoading({ commit }) {
    commit('TOGGLE_REPORT_DETAIL_LOADING')
  },
  getReportDetail({ commit }, data) {
    commit('GET_REPORT_DETAIL', data)
  },
  getReportRevisionDetail({ commit }, data) {
    commit('GET_REPORT_REVISION_DETAIL', data)
  },
  getReportRecap({ commit }, data) {
    commit('GET_REPORT_RECAP', data)
  },
  getReportRevisionRecap({ commit }, data) {
    commit('GET_REPORT_REVISION_RECAP', data)
  },
  toggleDetailLoading({ commit }) {
    commit('TOGGLE_DETAIL_LOADING')
  },
  toggleRecapLoading({ commit }) {
    commit('TOGGLE_RECAP_LOADING')
  },
  toggleRevisionDetailLoading({ commit }) {
    commit('TOGGLE_REVISION_DETAIL_LOADING')
  },
  toggleRevisionRecapLoading({ commit }) {
    commit('TOGGLE_REVISION_RECAP_LOADING')
  }
}

export default {
  state, mutations, actions
}
