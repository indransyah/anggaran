const state = {
  alert: {
    isActive: false,
    type: '',
    title: '',
    message: ''
  }
}

const mutations = {
  TOGGLE_ALERT(state) {
    state.alert.isActive = !state.alert.isActive
  },
  SET_ALERT(state, options) {
    state.alert = {
      ...state.alert,
      isActive: true,
      type: options.type,
      title: options.title,
      message: options.message
    }
  }
}

const actions = {
  toggleAlert({ commit }) {
    commit('TOGGLE_ALERT')
  },
  setAlert({ commit }, options) {
    commit('SET_ALERT', options)
  }
}

export default {
  state, mutations, actions
}
