const state = {
  isLoading: false,
  bills: [],
  prev_page_url: null,
  next_page_url: null
}

const mutations = {
  TOGGLE_BILL_LOADING(state) {
    state.isLoading = !state.isLoading
  },
  GET_BILLS(state, data) {
    state.bills = [ ...data ]
  },
  GET_BILLS_PAGINATION(state, data) {
    state.prev_page_url = data.prev_page_url
    state.next_page_url = data.next_page_url
  }
}

const actions = {
  toggleBillLoading({ commit }) {
    commit('TOGGLE_BILL_LOADING')
  },
  getBills({ commit }, data) {
    commit('GET_BILLS', data)
  },
  getBillsPagination({ commit }, data) {
    commit('GET_BILLS_PAGINATION', data)
  }
}

export default {
  state, mutations, actions
}
