const state = {
  isLoading: false,
  departements: [],
  prev_page_url: null,
  next_page_url: null
}

const mutations = {
  TOGGLE_DEPARTEMENT_LOADING(state) {
    state.isLoading = !state.isLoading
  },
  GET_DEPARTEMENTS(state, data) {
    state.departements = [ ...data ]
  },
  GET_DEPARTEMENTS_PAGINATION(state, data) {
    state.prev_page_url = data.prev_page_url
    state.next_page_url = data.next_page_url
  }
}

const actions = {
  toggleDepartementLoading({ commit }) {
    commit('TOGGLE_DEPARTEMENT_LOADING')
  },
  getDepartements({ commit }, data) {
    commit('GET_DEPARTEMENTS', data)
  },
  getDepartementsPagination({ commit }, data) {
    commit('GET_DEPARTEMENTS_PAGINATION', data)
  }
}

export default {
  state, mutations, actions
}
