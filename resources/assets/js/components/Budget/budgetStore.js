import _ from 'lodash'

const state = {
  isLoading: false,
  budgets: [],
  input: {
    nama: null,
    volume: null,
    satuan: null,
    harga: null,
    total: null,
    lokasi: null
  },
  prev_page_url: null,
  next_page_url: null,
  departments: [],
  isDescendantLoading: false,
  descendantBudgets: []
}

function getBudget(budgets, budgetId) {
  const index = budgets.findIndex(budget => {
    return budget.id === budgetId
  })
  if (index > -1) {
    return {
      index,
      data: budgets[index]
    }
  } else {
    return {}
  }
}

const mutations = {
  TOGGLE_BUDGET_LOADING(state) {
    state.isLoading = !state.isLoading
  },
  RESET_BUDGETS(state) {
    state.budgets = []
  },
  GET_BUDGETS(state, data) {
    state.budgets = [ ...data ]
  },
  GET_BUDGETS_PAGINATION(state, data) {
    state.prev_page_url = data.prev_page_url
    state.next_page_url = data.next_page_url
  },
  GET_DEPARTMENTS(state, data) {
    state.departments = [ ...data ]
  },
  TOGGLE_DESCENDANT_BUDGET_LOADING(state) {
    state.isDescendantLoading = !state.isDescendantLoading
  },
  GET_DESCENDANT_BUDGETS(state, data) {
    state.descendantBudgets = [ ...data ]
  },
  TOGGLE_DESCENDANT_BUDGET_STATUS_LOADING(state, budgetId) {
    let editedBudget = getBudget(state.descendantBudgets, budgetId)
    if (!_.isEmpty(editedBudget)) {
      editedBudget.data.isLoading = !editedBudget.data.isLoading
      state.descendantBudgets = [
        ...state.descendantBudgets.slice(0, editedBudget.index),
        editedBudget.data,
        ...state.descendantBudgets.slice(editedBudget.index + 1)
      ]
    }
  },
  UPDATE_DESCENDANT_BUDGET_STATUS(state, data) {
    let editedBudget = getBudget(state.descendantBudgets, data.budgetId)
    if (!_.isEmpty(editedBudget)) {
      editedBudget.data.status = data.status
      state.descendantBudgets = [
        ...state.descendantBudgets.slice(0, editedBudget.index),
        editedBudget.data,
        ...state.descendantBudgets.slice(editedBudget.index + 1)
      ]
    }
  }
}

const actions = {
  toggleBudgetLoading({ commit }) {
    commit('TOGGLE_BUDGET_LOADING')
  },
  resetBudgets({ commit }) {
    commit('RESET_BUDGETS')
  },
  getBudgets({ commit }, data) {
    commit('GET_BUDGETS', data)
  },
  getBudgetsPagination({ commit }, data) {
    commit('GET_BUDGETS_PAGINATION', data)
  },
  getDepartments({ commit }, data) {
    commit('GET_DEPARTMENTS', data)
  },
  toggleDescendantBudgetLoading({ commit }) {
    commit('TOGGLE_DESCENDANT_BUDGET_LOADING')
  },
  getDescendantBudgets({ commit }, data) {
    commit('GET_DESCENDANT_BUDGETS', data)
  },
  updateDescendantBudgetStatus({ commit }, data) {
    commit('UPDATE_DESCENDANT_BUDGET_STATUS', data)
  },
  toggleDescendantBudgetStatusLoading({ commit }, budgetId) {
    commit('TOGGLE_DESCENDANT_BUDGET_STATUS_LOADING', budgetId)
  }
}

export default {
  state, mutations, actions
}
