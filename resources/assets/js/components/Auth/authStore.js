const state = {
  isLoading: false
}

const mutations = {
  TOGGLE_LOADING(state) {
    state.isLoading = !state.isLoading
  }
}

const actions = {
  toggleLoading({ commit }) {
    commit('TOGGLE_LOADING')
  }
}

export default {
  state, mutations, actions
}
