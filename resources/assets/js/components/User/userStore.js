const state = {
  isLoading: false,
  users: [],
  prev_page_url: null,
  next_page_url: null
}

const mutations = {
  TOGGLE_USER_LOADING(state) {
    state.isLoading = !state.isLoading
  },
  GET_USERS(state, data) {
    state.users = [ ...data ]
  },
  GET_USERS_PAGINATION(state, data) {
    state.prev_page_url = data.prev_page_url
    state.next_page_url = data.next_page_url
  }
}

const actions = {
  toggleUserLoading({ commit }) {
    commit('TOGGLE_USER_LOADING')
  },
  getUsers({ commit }, data) {
    commit('GET_USERS', data)
  },
  getUsersPagination({ commit }, data) {
    commit('GET_USERS_PAGINATION', data)
  }
}

export default {
  state, mutations, actions
}
