const state = {
  isReportRecapLoading: false,
  isReportDetailLoading: false,
  reportDetail: [],
  reportRecap: []
}

const mutations = {
  TOGGLE_REPORT_RECAP_LOADING(state) {
    state.isReportRecapLoading = !state.isReportRecapLoading
  },
  TOGGLE_REPORT_DETAIL_LOADING(state) {
    state.isReportDetailLoading = !state.isReportDetailLoading
  },
  GET_REPORT_DETAIL(state, data) {
    state.reportDetail = [ ...data ]
  },
  GET_REPORT_RECAP(state, data) {
    state.reportRecap = [ ...data ]
  }
}

const actions = {
  toggleReportRecapLoading({ commit }) {
    commit('TOGGLE_REPORT_RECAP_LOADING')
  },
  toggleReportDetailLoading({ commit }) {
    commit('TOGGLE_REPORT_DETAIL_LOADING')
  },
  getReportDetail({ commit }, data) {
    commit('GET_REPORT_DETAIL', data)
  },
  getReportRecap({ commit }, data) {
    commit('GET_REPORT_RECAP', data)
  }
}

export default {
  state, mutations, actions
}
