import Vue from 'vue'
import Vuex from 'vuex'

import authStore from './components/Auth/authStore'
import billStore from './components/Bill/billStore'
import budgetStore from './components/Budget/budgetStore'
import departmentStore from './components/Department/departmentStore'
import userStore from './components/User/userStore'
import reportStore from './components/Report/reportStore'
import appStore from './components/appStore'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    appStore,
    authStore,
    billStore,
    budgetStore,
    departmentStore,
    userStore,
    reportStore
  },
  strict: debug
})
