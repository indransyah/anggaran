<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Budget extends Model
{
    //
    public static $rules = [
      'nama' => 'required|string',
      'volume' => 'required|integer',
      'satuan' => 'required|string',
      'harga' => 'required|integer',
      'anggaran' => 'required|integer',
      'lokasi' => 'string',
      'id_rekening' => 'required|integer'
    ];

    protected $fillable = [
        'name', 'volume', 'unit', 'price', 'total', 'location', 'bill_id', 'departement_id', 'period_id', 'revision_id'
    ];

    public function departement()
    {
      return $this->belongsTo('App\Departement');
    }

    public function bill()
    {
      return $this->belongsTo('App\Bill');
    }

    public function period()
    {
      return $this->belongsTo('App\Period');
    }

    public function notes()
    {
      return $this->hasMany('App\Note');
    }

    public function base()
    {
      return $this->belongsTo('App\Budget', 'revision_id');
    }

    public function revision()
    {
      return $this->hasOne('App\Budget', 'revision_id');
    }
}
