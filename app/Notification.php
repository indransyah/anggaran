<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    //

    public static $rules = [
      'departement_from' => 'required|integer',
      'departement_to' => 'required|integer',
      'notification_type' => 'required|integer'
    ];

    protected $fillable = [
        'departement_from', 'departement_to', 'notification_type'
    ];

    public function departementFrom()
    {
      return $this->belongsTo('App\Departement', 'departement_from');
    }

    public function departementTo()
    {
      return $this->belongsTo('App\Departement', 'departement_to');
    }

    public function notificationType()
    {
      return $this->belongsTo('App\NotificationType', 'notification_type');
    }
}
