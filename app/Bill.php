<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Bill extends Model
{
    use NodeTrait;

    //
    public static $rules = [
      'kode' => 'required|string',
      'nama' => 'required|string',
      'rekening_atas' => 'integer'
    ];

    protected $fillable = [
      'code', 'name', 'parent_id'
    ];
    
    protected $hidden = [
        '_lft', '_rgt', 'parent_id', 'created_at', 'updated_at'
    ];

    public function budget()
    {
      return $this->hasMany('App\Budget');
    }
}
