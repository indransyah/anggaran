<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationType extends Model
{
    //
    public $timestamps = false;

    public static $rules = [
      'name' => 'required|string',
      'message' => 'required|string'
    ];

    protected $fillable = [
        'name', 'message'
    ];
}
