<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Period extends Model
{
    //
    public static $rules = [
      'nama' => 'required|string',
      'tahun' => 'integer'
    ];

    protected $fillable = [
      'name', 'year', 'is_active'
    ];
    
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function budget()
    {
      return $this->hasMany('App\Budget');
    }
}
