<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    public static $rules = [
      'catatan' => 'required|string'
    ];

    protected $fillable = [
        'note', 'budget_id', 'created_by'
    ];

    public function budget()
    {
      return $this->belongsTo('App\Budget');
    }

    public function user()
    {
      return $this->belongsTo('App\User', 'created_by');
    }
}
