<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    //

    protected $fillable = [
        'name', 'volume', 'unit', 'price', 'total', 'location', 'bill_id', 'departement_id'
    ];

    public function departement()
    {
      return $this->belongsTo('App\Departement');
    }

    public function bill()
    {
      return $this->belongsTo('App\Bill');
    }
}
