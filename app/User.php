<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Kodeine\Acl\Traits\HasRole;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, HasRole, Notifiable;

    public static $rules = [
      'nama' => 'required|string',
      'username' => 'required|string',
      'password' => 'required|string'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'password', 'departement_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function findForPassport($username) {
      return $this->where('username', $username)->first();
    }
    
    public function departement()
    {
        return $this->belongsTo('App\Departement');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    public function notes()
    {
        return $this->hasMany('App\Note');
    }
}
