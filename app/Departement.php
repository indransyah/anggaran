<?php

namespace App;
use Kalnoy\Nestedset\NodeTrait;

use Illuminate\Database\Eloquent\Model;

class Departement extends Model
{
    //
    use NodeTrait;

    public static $rules = [
      'nama' => 'required|string',
      'id_departemen_atasan' => 'integer'
    ];

    protected $fillable = [
        'code', 'name', 'description', 'parent_id'
    ];

    protected $hidden = [
        '_lft', '_rgt', 'parent_id', 'created_at', 'updated_at'
    ];

    public function user()
    {
        return $this->hasMany('App\User');
    }

    public function budget()
    {
        return $this->hasMany('App\Budget');
    }

    public function coordinator()
    {
        return $this->hasOne('App\Coordinator');
    }
}
