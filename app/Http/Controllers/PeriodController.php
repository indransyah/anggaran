<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StorePeriodPost;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

// Model
use App\Period;
use App\Budget;
use App\Departement;
use App\Notification;
use App\NotificationType;

class PeriodController extends Controller
{
    protected $errorMessages = [
      'store' => 'Tidak dapat manambah periode!',
      'show' => 'Periode tidak dapat ditemukan!',
      'update' => 'Periode tidak dapat disunting!',
      'destroy' => 'Periode tidak dapat dihapus!',
    ];

    public function active()
    {
        $period = Period::where('is_active', true)->first();
        return response($period);
    }

    public function check()
    {
        $period = Period::where('is_active', true)->count();
        return response($period);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $periods = Period::orderBy('created_at', 'desc')->get();
        return response($periods);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\StorePeriodPost  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePeriodPost $request)
    {
        try {
          DB::beginTransaction();
          Departement::where('is_fixed', true)->update(['is_fixed' => false]);
          Period::where('is_active', true)->update(['is_active' => false]);
          $period = Period::create([
              'name' => $request->input('nama'),
              'year' => $request->input('tahun'),
              'is_active' => true
          ]);
          $departement = Departement::findOrFail($request->user()->departement_id);
          $notificationType = NotificationType::where('name', 'newPeriod')->first();
          Notification::create([
            'departement_from' => $departement->id,
            'departement_to' => $departement->id,
            'notification_type' => (is_null($notificationType)) ? null : $notificationType->id
          ]);
        } catch (\Exception $e) {
          Log::error($e);
          DB::rollBack();
          return response(['message' => $this->errorMessages['store']], 400);
        }
        DB::commit();
        return response($period);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
          $period = Period::findOrFail($id);
        } catch (\Exception $e) {
          return response(['message' => $this->errorMessages['show']], 400);
        }
        return response($period);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\StorePeriodPost  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePeriodPost $request, $id)
    {
        try {
          $period = Period::findOrFail($id);
          $period->name = $request->input('nama');
          $period->year = $request->input('tahun');
          $period->save();
        } catch (\Exception $e) {
          Log::error($e);
          return response(['message' => $this->errorMessages['update']], 400);
        }
          return response($period);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
          $period = Period::findOrFail($id);
          $period->delete();
        } catch (\Exception $e) {
          Log::error($e);
          return response(['message' => $this->errorMessages['destroy']], 400);
        }
        return response([]);
    }

    public function revision(Request $request, $id)
    {
        try {
          DB::beginTransaction();
          $period = Period::findOrFail($id);
          $period->has_revision = true;
          $period->save();
          $budgets = Budget::where('period_id', $period->id)->get();
          foreach ($budgets as $key => $value) {
            $revisionBudget = Budget::create([
              'name' => $value->name,
              'volume' => $value->volume,
              'unit' => $value->unit,
              'price' => $value->price,
              'total' => $value->total,
              'location' => $value->location,
              'bill_id' => $value->bill_id,
              'period_id' => $value->period_id,
              'departement_id' => $value->departement_id,
              'revision_id' => $value->id,
              'created_by' => $value->created_by,
              'updated_by' => $value->updated_by
            ]);
          }
          $departement = Departement::findOrFail($request->user()->departement_id);
          $notificationType = NotificationType::where('name', 'revPeriod')->first();
          Notification::create([
            'departement_from' => $departement->id,
            'departement_to' => $departement->id,
            'notification_type' => (is_null($notificationType)) ? null : $notificationType->id
          ]);
        } catch (\Exception $e) {
          DB::rollBack();
          Log::error($e);
          return response(['message' => 'Tidak dapat membuat anggaran perubahan'], 400);
        }
        DB::commit();
        return response([]);
    }
}
