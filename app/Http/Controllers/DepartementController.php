<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreDepartementPost;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

// Model
use App\Departement;
use App\Notification;
use App\NotificationType;

class DepartementController extends Controller
{
    protected $errorMessages = [
      'store' => 'Tidak dapat manambah unit bagian!',
      'show' => 'Departemen tidak dapat ditemukan!',
      'update' => 'Departemen tidak dapat disunting!',
      'destroy' => 'Departemen tidak dapat dihapus!',
    ];
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      if (is_null($request->input('keyword'))) {
        $departements = Departement::where('id', '!=', 1)->orderBy('created_at', 'desc')->simplePaginate(10);
      } else {
        $keyword = $request->input('keyword');
        $departements = Departement::where('id', '!=', 1)->where('name', 'like', '%'.$keyword.'%')->orderBy('created_at', 'desc')->simplePaginate(10);
        $departements->appends(['keyword' => $keyword]);
      }
      foreach ($departements as $key => $value) {
        $parent = null;
        if ($value->parent_id !== null) {
          $parent = Departement::find($value->parent_id);
        }
        $value->setAttribute('parent', $parent);
      }
      return response($departements);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function descendant(Request $request)
    {
      $departements = Departement::where('id', $request->user()->departement_id)->orWhere('parent_id', $request->user()->departement_id)->get();
      return response($departements);
    }

    public function ancestor(Request $request)
    {
      $departements = Departement::ancestorsOf($request->user()->departement_id);
      return response($departements);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreDepartementPost  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDepartementPost $request)
    {
        //
        try {
          // Generate new code
          $departmentId = (empty($request->input('id_departemen_atasan'))) ? 1 : $request->input('id_departemen_atasan');
          $lastSlibing = Departement::where('parent_id', $departmentId)->orderBy('id', 'desc')->first();
          if (is_null($lastSlibing)) {
            $tempDepartment = Departement::find($departmentId);
            $newCode = $tempDepartment->code.'.1';
          } else {
            $code = explode('.', $lastSlibing->code);
            $code[count($code) - 1] += 1;
            $newCode = implode('.', $code);
          }

          // Save new departement
          $departement = Departement::create([
            'code' => $newCode,
            'name' => $request->input('nama'),
            'description' => $request->input('deskripsi'),
            'parent_id' => $request->input('id_departemen_atasan')
          ]);
        } catch (\Exception $e) {
          Log::error($e);
          return response(['message' => $this->errorMessages['store']], 400);
        }
        return response($departement);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
          $departement = Departement::findOrFail($id);
          return response($departement);
        } catch (\Exception $e) {
          return response(['message' => $this->errorMessages['show']], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\StoreDepartementPost  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreDepartementPost $request, $id)
    {
        //
        try {
          $departement = Departement::findOrFail($id);
          $departement->name = $request->input('nama');
          $departement->description = $request->input('deskripsi');
          // $departement->parent_id = $request->input('id_departemen_atasan');
          $departement->save();
          return response($departement);
        } catch (\Exception $e) {
          Log::error($e);
          return response(['message' => $this->errorMessages['update']], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
          $departement = Departement::findOrFail($id);
          $departement->delete();
          return response([]);
        } catch (\Exception $e) {
          Log::error($e);
          return response(['message' => $this->errorMessages['destroy']]);
        }
    }

    public function freeze(Request $request)
    {
      DB::beginTransaction();
      try {
        $departement = Departement::findOrFail($request->user()->departement_id);
        $departement->is_fixed = true;
        $departement->save();
        $notificationType = NotificationType::where('name', 'request')->first();
        Notification::create([
          'departement_from' => $departement->id,
          'departement_to' => $departement->parent_id,
          'notification_type' => (is_null($notificationType)) ? null : $notificationType->id
        ]);
      } catch (Exception $e) {
        Log::error($e);
            return response(['message' => 'Tidak dapat mengajukan anggaran']);        
        }
    }
}
