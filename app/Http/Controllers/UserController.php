<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreUserPost;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

// Model
use App\User;
use App\Departement;

class UserController extends Controller
{
    protected $errorMessages = [
      'store' => 'Tidak dapat manambah pengguna!',
      'show' => 'Pengguna tidak dapat ditemukan!',
      'update' => 'Pengguna tidak dapat disunting!',
      'destroy' => 'Pengguna tidak dapat dihapus!',
    ];

    /**
     * Display a user info.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function info(Request $request)
    {
      $user = User::where('id', $request->user()->id)->with('departement', 'roles')->first();
      $descendants = Departement::descendantsOf($request->user()->departement_id);
      $user->setAttribute('descendants', $descendants);
      return response($user);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      if (is_null($request->input('keyword'))) {
        $users = User::with('departement', 'roles')->simplePaginate(10);
      } else {
        $keyword = $request->input('keyword');
        $users = User::with('departement', 'roles')->where('name', 'like', '%'.$keyword.'%')->simplePaginate(10);
        $users->appends(['keyword' => $keyword]);
      }
      return response($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreUserPost  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserPost $request)
    {
        //
        if ($request->input('id_departemen') === 1) {
          return response(['message' => 'Tidak dapat menambah lebih dari satu akun direktur!'], 400);
        }

        try {
          // Save new departement
          $user = User::create([
            'name' => $request->input('nama'),
            'username' => $request->input('username'),
            'password' => Hash::make($request->input('password')),
            'departement_id' => $request->input('id_departemen')
          ]);
          if ($request->input('id_departemen') === 2) {
            $user->assignRole('developer');
          } else {
            $user->assignRole('staff');
          }
        } catch (\Exception $e) {
          Log::error($e);
          return response(['message' => $this->errorMessages['store']], 400);
        }
        return response($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
          $user = User::findOrFail($id);
          return response($user);
        } catch (\Exception $e) {
          return response(['message' => $this->errorMessages['show']], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try {
          $user = User::findOrFail($id);
          $user->name = $request->input('nama');
          $user->username = $request->input('username');
          if (!empty($request->input('password')) && !empty($request->input('passwordBaru'))) {
            if (!Hash::check($request->input('password'), $user->password)) {
              return response(['message' => 'Password Anda saat ini salah!'], 400);
            }
            $user->password = Hash::make($request->input('passwordBaru'));
          }
          $user->save();
          return response($user);
        } catch (\Exception $e) {
          Log::error($e);
          return response(['message' => $this->errorMessages['update']], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
          $user = User::findOrFail($id);
          $user->delete();
          return response([]);
        } catch (\Exception $e) {
          Log::error($e);
          return response(['message' => $this->errorMessages['destroy']]);
        }
    }

    public function username(Request $request)
    {
        $user = User::where('username', $request->input('username'))->first();
        if (is_null($user)) {
            return response(1);
        }
        return response(0);
    }
}
