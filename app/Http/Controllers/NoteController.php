<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreNotePost;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

// Model
use App\Note;
use App\Budget;
use App\Notification;
use App\NotificationType;

class NoteController extends Controller
{
	protected $errorMessages = [
      'store' => 'Tidak dapat manambah catatan!'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreNotePost  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNotePost $request, $id)
    {
        DB::beginTransaction();
        try {
        	$budget = Budget::findOrFail($id);
        	$budget->is_rejected = true;
        	$budget->save();
            /*
            $departement = Departement::findOrFail($budget->departement_id);
            if ($departement->is_fixed) {
                $userDepartment = Departement::findOrFail($request->user()->departement_id);
                $notificationType = NotificationType::where('name', 'reject')->first();
                Notification::create([
                  'departement_from' => $userDepartment->id,
                  'departement_to' => $departement->id,
                  'notification_type' => (is_null($notificationType)) ? null : $notificationType->id
                ]);
                $departement->is_fixed = false;
                $departement->save();
            }
            */
            $note = Note::create([
                'note' => $request->input('catatan'),
                'created_by' => $request->user()->id,
                'budget_id' => $id
            ]);
        } catch (\Exception $e) {
        	Log::error($e);
            DB::rollBack();
          	return response(['message' => $this->errorMessages['store']], 400);
        }
        DB::commit();
        return response([]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
