<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreCategoryPost;
use Illuminate\Support\Facades\Log;

// Model
use App\Category;

class CategoryController extends Controller
{
    protected $errorMessages = [
      'store' => 'Tidak dapat manambah kategory rekening!',
      'show' => 'Kategori rekening tidak dapat ditemukan!',
      'update' => 'Kategori rekening tidak dapat disunting!',
      'destroy' => 'Kategori rekening tidak dapat dihapus!',
    ];

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (is_null($request->input('keyword'))) {
          $categories = Category::orderBy('created_at', 'desc')->simplePaginate(10);
        } else {
          $keyword = $request->input('keyword');
          $categories = Category::where('name', $keyword)->orWhere('name', 'like', '%'.$keyword.'%')->orderBy('created_at', 'desc')->simplePaginate(10);
          $categories->appends(['keyword' => $keyword]);
        }
        return response($categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Requests\StoreCategoryPost $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCategoryPost $request)
    {
        try {
          $category = Category::create([
            'code' => $request->input('kode'),
            'name' => $request->input('nama')
          ]);
        } catch (\Exception $e) {
          if (\App::environment() === 'local') {
            Log::error($e);
          }
          return response(['message' => $this->errorMessages['store']], 400);
        }
        return response($category);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
          $category = Category::findOrFail($id);
          return response($category);
        } catch (\Exception $e) {
          return response(['message' => $this->errorMessages['show']], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Requests\StoreCategoryPost $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreCategoryPost $request, $id)
    {
        try {
          $category = Category::findOrFail($id);
          $category->code = $request->input('kode');
          $category->name = $request->input('nama');
          $category->save();
          return response($category);
        } catch (\Exception $e) {
          if (\App::environment() === 'local') {
            Log::error($e);
          }
          return response(['message' => $this->errorMessages['update']], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
          $category = Category::findOrFail($id);
          $category->delete();
          return response([]);
        } catch (\Exception $e) {
          if (\App::environment() === 'local') {
            Log::error($e);
          }
          return response(['message' => $this->errorMessages['destroy']], 400);
        }
    }
}
