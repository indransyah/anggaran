<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreBudgetPost;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

// Model
use App\Budget;
use App\Report;
use App\Departement;
use App\Notification;
use App\NotificationType;
use App\Period;

class BudgetController extends Controller
{
    protected $errorMessages = [
      'store' => 'Tidak dapat manambah rancangan anggaran!',
      'show' => 'Rancangan anggaran tidak dapat ditemukan!',
      'update' => 'Rancangan anggaran tidak dapat disunting!',
      'destroy' => 'Rancangan anggaran tidak dapat dihapus!',
      'approve' => 'Rancangan anggaran tidak dapat disetujui!',
      'reject' => 'Rancangan anggaran tidak dapat ditolak!',
      'submit' => 'Rancangan anggaran tidak dapat diajukan!',
      'descendant' => 'Tidak dapat mengambil rancangan anggaran!',
      'period' => 'Periode yang aktif tidak ditemukan!'
    ];
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $children = Departement::descendantsOf($request->user()->departement_id)->pluck('id')->toArray();
      array_unshift($children, $request->user()->departement_id);
      if (is_null($request->input('keyword'))) {
        // $budgets = Budget::where('used_by', $request->user()->departement_id)->where('status', 'approved')->with('departement', 'bill')->orderBy('created_at', 'desc')->simplePaginate(10);
        // $budgets = Budget::with('departement', 'bill')->where('created_by', $request->user()->id)->whereIn('departement_id', $children)->orderBy('created_at', 'desc')->simplePaginate(10);
        $budgets = Budget::with('departement', 'bill')->whereIn('departement_id', $children)->orderBy('created_at', 'desc')->simplePaginate(10);
      } else {
        $keyword = $request->input('keyword');
        // $budgets = Budget::with('departement', 'bill')->where('used_by', $request->user()->departement_id)->where('status', 'approved')->where('name', 'like', '%'.$keyword.'%')->orderBy('created_at', 'desc')->simplePaginate(10);
        // $budgets = Budget::with('departement', 'bill')->where('created_by', $request->user()->id)->where('name', 'like', '%'.$keyword.'%')->whereIn('departement_id', $children)->orderBy('created_at', 'desc')->simplePaginate(10);
        $budgets = Budget::with('departement', 'bill')->where('name', 'like', '%'.$keyword.'%')->whereIn('departement_id', $children)->orderBy('created_at', 'desc')->simplePaginate(10);
        $budgets->appends(['keyword' => $keyword]);
      }
      return response($budgets);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function submit(Request $request)
    {
      try {
        DB::beginTransaction();
        // Request the budgets
        $departement = Departement::findOrFail($request->user()->departement_id);
        Budget::where('used_by', $departement->id)->update(['status' => 'requested', 'requested_to' => $departement->parent_id]);

        // Save budgets to report
        $budgets = Budget::where(['used_by' => $departement->id, 'requested_to' => $departement->parent_id])->get();
        $data = $budgets->map(function($budget, $key) {
          return [
            'name' => $budget['name'],
            'volume' => $budget['volume'],
            'unit' => $budget['unit'],
            'price' => $budget['price'],
            'total' => $budget['total'],
            'location' => $budget['location'],
            'period_id' => $budget['period_id'],
            'bill_id' => $budget['bill_id'],
            'departement_id' => $budget['departement_id']
          ];
        });
        $reports = Report::insert($data->toArray());

        // Save notification
        $notification = NotificationType::where('name', 'request')->first();
        Notification::create([
          'departement_from' => $departement->id,
          'departement_to' => $departement->parent_id,
          'notification_type' => (is_null($notification)) ? null : $notification->id
        ]);
      } catch (\Exception $e) {
        Log::error($e);
        DB::rollBack();
        return response(['message' => $this->errorMessages['submit']], 400);
      }
      DB::commit();
      return response([]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function approval(Request $request)
    {
      $children = \App\Departement::descendantsOf($request->user()->departement_id)->pluck('id')->toArray();
      // array_unshift($children, $request->user()->departement_id);
      if (is_null($request->input('keyword'))) {
        $budgets = Budget::orderBy('created_at', 'desc')->whereIn('departement_id', $children)->simplePaginate(10);
      } else {
        $keyword = $request->input('keyword');
        $budgets = Budget::where('name', $keyword)->orWhere('name', 'like', '%'.$keyword.'%')->whereIn('departement_id', $children)->orderBy('created_at', 'desc')->simplePaginate(10);
        $budgets->appends(['keyword' => $keyword]);
      }
      return response($budgets);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function descendant(Request $request, $id)
    {
      try {
        $period = Period::where('is_active', true)->first();
        if (is_null($period)) {
          return response(['message' => $this->errorMessages['period']], 400);
        }
        $condition = '=';
        if ($period->has_revision) {
          $condition = '<>';
        }
        if ($request->user()->departement_id === intval($id)) {
        $budgets = Budget::with('departement', 'bill')->with(['notes' => function($q) {
          $q->with('user.departement')->orderBy('created_at', 'DESC');
        }])->where('departement_id', $id)->where('period_id', $period->id)->where('revision_id', $condition, NULL)->orderBy('bill_id')->get()->toArray();
        if ($period->has_revision) {
          $additionalBudgets = Budget::with('departement', 'bill')->with(['notes' => function($q) {
            $q->with('user.departement')->orderBy('created_at', 'DESC');
          }])->where('departement_id', $id)->where('period_id', $period->id)->where('is_additional', true)->orderBy('bill_id')->get()->toArray();
          return response(array_merge($budgets, $additionalBudgets));
        }
        return response($budgets);
        }
        $children = Departement::descendantsOf($request->user()->departement_id)->pluck('id')->toArray();
        array_unshift($children, $request->user()->departement_id);
        if (in_array($id, $children)) {
          $descendant = Departement::descendantsOf($id)->pluck('id')->toArray();
          array_unshift($descendant, intval($id));
          $budgets = Budget::with('departement', 'bill')->with(['notes' => function($q) {
            $q->with('user.departement')->orderBy('created_at', 'DESC');
          }])->whereIn('departement_id', $descendant)->where('period_id', $period->id)->where('revision_id', $condition, NULL)->orderBy('bill_id')->get()->toArray();
          if ($period->has_revision) {
            $additionalBudgets = Budget::with('departement', 'bill')->with(['notes' => function($q) {
              $q->with('user.departement')->orderBy('created_at', 'DESC');
            }])->where('departement_id', $id)->where('period_id', $period->id)->where('is_additional', true)->orderBy('bill_id')->get()->toArray();
            return response(array_merge($budgets, $additionalBudgets));
          }
          return response($budgets);
        }
        return response([]);
      } catch (\Exception $e) {
        Log::error($e);
        return response(['message' => $this->errorMessages['descendant']], 400);
      }
      /*
      if ($request->user()->departement_id === intval($id)) {
        $budgets = Budget::with('departement', 'bill')->with(['notes' => function($q) {
          $q->with('user.departement')->orderBy('created_at', 'DESC');
        }])->where('departement_id', $id)->where('revision_id', NULL)->orderBy('bill_id')->get();
        return response($budgets);
      }
      $children = Departement::descendantsOf($request->user()->departement_id)->pluck('id')->toArray();
      array_unshift($children, $request->user()->departement_id);
      if (in_array($id, $children)) {
        // $budgets = Budget::with('departement', 'bill')->where('status', 'requested')->where('used_by', $id)->where('requested_to', $request->user()->departement_id)->orderBy('bill_id')->get();
        $descendant = Departement::descendantsOf($id)->pluck('id')->toArray();
        array_unshift($descendant, intval($id));
        $budgets = Budget::with('departement', 'bill')->with(['notes' => function($q) {
          $q->with('user.departement')->orderBy('created_at', 'DESC');
        }])->whereIn('departement_id', $descendant)->where('revision_id', NULL)->orderBy('bill_id')->get();
        return response($budgets);
      } else {
        return response([]);
      }
      */
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBudgetPost  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBudgetPost $request)
    {
        //
        try {
          $period = Period::where('is_active', true)->first();
          if ($period === null) {
            return response(['message' => 'Tidak terdapat periode yang aktif!'], 400);
          }
          // Save new departement
          $budget = Budget::create([
            'name' => $request->input('nama'),
            'volume' => $request->input('volume'),
            'unit' => $request->input('satuan'),
            'price' => $request->input('harga'),
            'total' => $request->input('anggaran'),
            'location' => $request->input('lokasi'),
            'bill_id' => $request->input('id_rekening'),
            'period_id' => $period->id
          ]);
        } catch (\Exception $e) {
          Log::error($e);
          return response(['message' => $this->errorMessages['store']], 400);
        }
        return response($budget);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function insert(Request $request)
    {
        //
        try {
          $period = Period::where('is_active', true)->first();
          if ($period === null) {
            return response(['message' => 'Tidak terdapat periode yang aktif!'], 400);
          }
          // Save new departement
          $data = $request->input();
          foreach ($data as $key => $value) {
            // $data[$key]['status'] = 'approved';
            $data[$key]['departement_id'] = $request->user()->departement_id;
            $data[$key]['period_id'] = $period->id;
            $data[$key]['created_at'] = date("Y-m-d H:i:s");
            $data[$key]['updated_at'] = date("Y-m-d H:i:s");
            // $data[$key]['used_by'] = $request->user()->departement_id;
            $data[$key]['created_by'] = $request->user()->id;
            $data[$key]['updated_by'] = $request->user()->id;
            $data[$key]['is_additional'] = false;
            if ($period->has_revision) {
              $data[$key]['is_additional'] = true;
            }
          }
          $budget = Budget::insert($data);
        } catch (\Exception $e) {
          Log::error($e);
          return response(['message' => $this->errorMessages['store']], 400);
        }
        return response([]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
          $budget = Budget::findOrFail($id);
          return response($budget);
        } catch (\Exception $e) {
          return response(['message' => $this->errorMessages['show']], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\StoreBudgetPost  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreBudgetPost $request, $id)
    {
        //
        try {
          $budget = Budget::findOrFail($id);
          $budget->name = $request->input('nama');
          $budget->volume = $request->input('volume');
          $budget->unit = $request->input('satuan');
          $budget->price = $request->input('harga');
          $budget->total = $request->input('anggaran');
          $budget->location = $request->input('lokasi');
          $budget->bill_id = $request->input('id_rekening');
          $budget->updated_by = $request->user()->id;
          $budget->is_rejected = false;
          $budget->save();
          return response($budget);
        } catch (\Exception $e) {
          Log::error($e);
          return response(['message' => $this->errorMessages['update']], 400);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve(Request $request, $id)
    {
        //
        try {
          $period = Period::where('is_active', true)->first();

          DB::beginTransaction();

          // Update budget
          $budget = Budget::findOrFail($id);
          $budget->status = 'approved';
          $budget->used_by = $request->user()->departement_id;
          $budget->requested_to = null;
          $budget->updated_by = $request->user()->id;
          $budget->save();

          // Save to report
          $report = Report::insert([
            'name' => $budget->name,
            'volume' => $budget->volume,
            'unit' => $budget->unit,
            'price' => $budget->price,
            'total' => $budget->total,
            'location' => $budget->location,
            'bill_id' => $budget->bill_id,
            'period_id' => $period->id,
            'departement_id' => $request->user()->departement_id
          ]);
        } catch (\Exception $e) {
          Log::error($e);
          DB::rollBack();
          return response(['message' => $this->errorMessages['approve']], 400);
        }
        DB::commit();
        return response($budget);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reject(Request $request, $id)
    {
        //
        try {
          $budget = Budget::findOrFail($id);
          $budget->status = 'rejected';
          $budget->used_by = $request->user()->departement_id;
          $budget->updated_by = $request->user()->id;
          $budget->save();
          return response($budget);
        } catch (\Exception $e) {
          Log::error($e);
          return response(['message' => $this->errorMessages['reject']], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
          $budget = Budget::findOrFail($id);
          $budget->delete();
          return response([]);
        } catch (\Exception $e) {
          Log::error($e);
          return response(['message' => $this->errorMessages['destroy']], 400);
        }
    }

    public function detail(Request $request, $id, $hasRevision = 0)
    {
      try {
        // $period = Period::where('is_active', true)->first();
        // if (is_null($period)) {
        //   return response(['message' => $this->errorMessages['period']], 400);
        // }
        $condition = '=';
        if ($hasRevision) {
          $condition = '<>';
        }
        $children = Departement::descendantsOf($request->user()->departement_id)->pluck('id')->toArray();
        array_unshift($children, $request->user()->departement_id);
        $reports = Budget::with('bill', 'departement', 'base')
          ->where('revision_id', $condition, NULL)
          ->where('is_additional', false)
          ->where('period_id', $id)
          ->where('is_rejected', false)
          ->whereIn('departement_id', $children)
          ->orderBy('bill_id', 'asc')
          ->get()
          ->toArray();
        if ($hasRevision) {
          $additionalReports = Budget::with('bill', 'departement', 'base')
            ->where('is_additional', true)
            ->where('period_id', $id)
            ->where('is_rejected', false)
            ->whereIn('departement_id', $children)
            ->orderBy('bill_id', 'asc')
            ->get()
            ->toArray();
          return response(array_merge($reports, $additionalReports));
        }
        return response($reports);
      } catch (\Exception $e) {
        Log::error($e);
        return response(['message' => 'Tidak dapat mengambil data'], 400);
      }
    }

    public function recap(Request $request, $id, $hasRevision = 0)
    {
      try {
        // $period = Period::where('is_active', true)->first();
        // if (is_null($period)) {
        //   return response(['message' => $this->errorMessages['period']], 400);
        // }
        $condition = '=';
        if ($hasRevision) {
          $condition = '<>';
        }
        $children = Departement::descendantsOf($request->user()->departement_id)->pluck('id')->toArray();
        array_unshift($children, $request->user()->departement_id);
        $reports = Budget::select('*', DB::raw('SUM(total) AS total'))
          ->with('bill', 'departement', 'base')
          ->where('revision_id', $condition, NULL)
          ->where('is_additional', false)
          ->where('period_id', $id)
          ->where('is_rejected', false)
          ->whereIn('departement_id', $children)
          ->groupBy('bill_id')
          ->orderByRaw('SUM(total) DESC')
          ->get()
          ->toArray();
        if ($hasRevision) {
          $additionalReports = Budget::select('*', DB::raw('SUM(total) AS total'))
            ->with('bill', 'departement', 'base')
            ->where('is_additional', true)
            ->where('period_id', $id)
            ->where('is_rejected', false)
            ->whereIn('departement_id', $children)
            ->groupBy('bill_id')
            ->orderByRaw('SUM(total) DESC')
            ->get()
            ->toArray();
          return response(array_merge($reports, $additionalReports));
        }
        return response($reports);
      } catch (\Exception $e) {
        Log::error($e);
        return response(['message' => 'Tidak dapat mengambil data'], 400);
      }
    }

    public function revDetail(Request $request, $id)
    {
        $children = Departement::descendantsOf($request->user()->departement_id)->pluck('id')->toArray();
        array_unshift($children, $request->user()->departement_id);
        $reports = Budget::with('bill', 'departement', 'revision')->where('period_id', $id)->where('is_rejected', false)->whereIn('departement_id', $children)->orderBy('bill_id', 'asc')->get();
        return response($reports);
    }
}
