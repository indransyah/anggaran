<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreCoordinatorPost;
use Illuminate\Support\Facades\Log;

// Model
use App\Coordinator;

class CoordinatorController extends Controller
{
    protected $errorMessages = [
      'store' => 'Tidak dapat manambah penanggungjawab!',
      'update' => 'Penanggungjawab tidak dapat disunting!'
    ];

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $departmentId)
    {
        $coordinator = Coordinator::where('departement_id', $departmentId)->first();
        return response($coordinator);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\StoreCoordinatorPost  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCoordinatorPost $request, $departmentId)
    {
        if ($departmentId != $request->user()->departement_id) {
          return response('', 401);
        }
        //
        try {
          $coordinator = Coordinator::create([
            'name' => $request->input('nama'),
            'position' => $request->input('jabatan'),
            'nip' => $request->input('nip'),
            'departement_id' => $departmentId
          ]);
        } catch (\Exception $e) {
          Log::error($e);
          return response(['message' => $this->errorMessages['store']], 400);
        }
        return response($coordinator);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\StoreCoordinatorPost  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreCoordinatorPost $request, $id)
    {
        try {
          $coordinator = Coordinator::findOrFail($id);
          if ($coordinator->departement_id !== $request->user()->departement_id) {
            return response('', 401);
          }
          $coordinator->name = $request->input('nama');
          $coordinator->position = $request->input('jabatan');
          $coordinator->nip = $request->input('nip');
          $coordinator->save();
        } catch (\Exception $e) {
          Log::error($e);
          return response(['message' => $this->errorMessages['update']], 400);
        }
        return response($coordinator);
    }
}
