<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

// Model
use App\Report;
use App\Bill;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function detail(Request $request, $id)
    {
        $reports = Report::with('bill', 'departement')->where('period_id', $id)->where('departement_id', $request->user()->departement_id)->orderBy('bill_id', 'asc')->get();
        return response($reports);
    }

    /**
     * Display a listing of the resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function recap(Request $request, $id)
    {
        $reports = Report::select('*', DB::raw('SUM(total) AS total'))->with('bill', 'departement')->where('period_id', $id)->where('departement_id', $request->user()->departement_id)->groupBy('bill_id')->orderByRaw('SUM(total) DESC')->get();
        // $reports = Bill::whereHas('budget', function($q) use ($request) {
        //     $q->select('bill_id', DB::raw('SUM(total) AS total'))->where('departement_id', $request->user()->departement_id)->groupBy('bill_id');
        // })->with(['budget' => function($q) use ($request) {
        //     $q->select('bill_id', DB::raw('SUM(total) AS total'))->where('departement_id', $request->user()->departement_id)->groupBy('bill_id');
        // }])->get();
        return response($reports);
    }
}
