<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreBillPost;
use Illuminate\Support\Facades\Log;

// Model
use App\Bill;

class BillController extends Controller
{
    protected $errorMessages = [
      'store' => 'Tidak dapat manambah rekening!',
      'show' => 'Rekening tidak dapat ditemukan!',
      'update' => 'Rekening tidak dapat disunting!',
      'destroy' => 'Rekening tidak dapat dihapus!',
    ];

    public function all(Request $request)
    {
      try {
        $bills = Bill::orderBy('code', 'asc')->get()->toTree();
      } catch (\Exception $e) {
        Log::error($e);
        return response(['message' => 'Tidak dapat mengambil data rekening!'], 400);
      }
      return response($bills);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (is_null($request->input('keyword'))) {
          $bills = Bill::orderBy('created_at', 'desc')->simplePaginate(100);
        } else {
          $keyword = $request->input('keyword');
          $bills = Bill::where('name', 'like', '%'.$keyword.'%')->orderBy('created_at', 'desc')->simplePaginate(100);
          $bills->appends(['keyword' => $keyword]);
        }
        foreach ($bills as $key => $value) {
          $parent = null;
          if ($value->parent_id !== null) {
            $parent = Bill::find($value->parent_id);
          }
          $value->setAttribute('parent', $parent);
        }
        return response($bills);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\StoreBillPost  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBillPost $request)
    {
        //
        try {
          $bill = Bill::create([
            'code' => $request->input('kode'),
            'name' => $request->input('nama'),
            'parent_id' => $request->input('rekening_atas')
          ]);
        } catch (\Exception $e) {
          Log::error($e);
          return response(['message' => $this->errorMessages['store']], 400);
        }
        return response($bill);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
          $bill = Bill::findOrFail($id);
          $parent = null;
          if ($bill->parent_id !== null) {
            $parent = Bill::find($bill->parent_id);
          }
          $bill->setAttribute('parent', $parent);
        } catch (\Exception $e) {
          return response(['message' => $this->errorMessages['show']], 400);
        }
        return response($bill);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\StoreBillPost  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreBillPost $request, $id)
    {
        try {
          $bill = Bill::findOrFail($id);
          $bill->code = $request->input('kode');
          $bill->name = $request->input('nama');
          $bill->parent_id = $request->input('rekening_atas');
          $bill->save();
          Bill::fixTree();
          return response($bill);
        } catch (\Exception $e) {
          Log::error($e);
          return response(['message' => $this->errorMessages['update']], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
          $bill = Bill::findOrFail($id);
          $bill->delete();
          return response([]);
        } catch (\Exception $e) {
          Log::error($e);
          return response(['message' => $this->errorMessages['destroy']]);
        }
    }

    public function latest($billId)
    {
      $bill = Bill::where('parent_id', $billId)->orderBy('id', 'desc')->first();
      return response($bill);
    }

    public function code(Request $request)
    {
        $bill = Bill::where('code', $request->input('code'))->first();
        if (is_null($bill)) {
            return response(1);
        }
        return response(0);
    }
}
