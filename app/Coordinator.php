<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coordinator extends Model
{
    //

    public static $rules = [
      'nama' => 'required|string',
      'jabatan' => 'required|string',
      'nip' => 'required|string'
    ];

    protected $fillable = [
        'name', 'position', 'nip', 'departement_id'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function departement()
    {
        return $this->belongsTo('App\Departement');
    }
}
